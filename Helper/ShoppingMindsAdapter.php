<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Helper;

use Magento\Framework\View\Element\Context;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Phrase;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Customer;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;

use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;

/**
 * Adapter class to map Magento 2 entities to request body arrays required by the Shopping Minds API endpoint
 *
 * Class ShoppingMindsAdapter
 *
 * @package Shoppingminds\Base\Helper
 */
class ShoppingMindsAdapter {
    # Fields used to generate key in product request body
    const PRODUCT_PUBLIC_FIELDS = ["name", "price","product_url"];
    # Attributes of product to exclude since they are included in the request body separately.
    const PRODUCT_ATTR_TO_EXCL = [
        'media_gallery',
        'swatch_image',
        'small_image',
        'image',
        'custom_layout',
        'page_layout',
        'category_ids',
    ];

    # Simple string holding the base url of the store for used as prefix for full url.
    protected $baseUrl;

    /** @var PriceCurrencyInterface  */
    protected $priceCurrency;
    /** @var CategoryRepository  */
    protected $categoryRepository;

    /** @var ShoppingmindsConfig  */
    protected $shoppingMindsConfig;

    public function __construct(
        Context $context,
        ShoppingmindsConfig $shoppingMindsConfig,
        CategoryRepository $categoryRepository,
        PriceCurrencyInterface $priceCurrency
    )
    {
        $this->baseUrl = $context->getUrlBuilder()->getBaseUrl();
        $this->priceCurrency = $priceCurrency;
        $this->categoryRepository = $categoryRepository;

        $this->shoppingMindsConfig = $shoppingMindsConfig;
    }

    /**
     * Map Product entity to request body array
     *
     * @param Product|ProductInterface $product
     * @return array
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function mapProductToRequestBody( $product )
    {
        $requestBody = [];

        /** @var Product $product */
        $requestBody['product_id'] = $product->getId();
        $requestBody['name'] = $product->getName();
        $requestBody['price'] = $this->formatPriceForShm( $product->getPrice() );
        $requestBody['product_url'] = $product->getProductUrl();
        $requestBody['image_url'] = $this->baseUrl . $product->getImage();
        $requestBody['key'] = $this->shoppingMindsConfig->getApiExportKey($requestBody, self::PRODUCT_PUBLIC_FIELDS);

        $requestBody['custom_data']['sku'] = $product->getSku();
        $requestBody['custom_data']['special_price']        = !is_null( $product->getSpecialPrice() )       ? $product->getSpecialPrice()       : "";
        $requestBody['custom_data']['special_price_from']   = !is_null( $product->getSpecialFromDate() )    ? $product->getSpecialFromDate()    : "";
        $requestBody['custom_data']['special_price_to']     = !is_null( $product->getSpecialToDate() )      ? $product->getSpecialToDate()      : "";
        $requestBody['custom_data']['categories'] = $this->getCategoryPathsForProduct( $product );

        $extraProductAttributes = $this->shoppingMindsConfig->getExtraProductAttributes();
        $attributes = $product->getAttributes();

        foreach ($attributes as $attribute) {
            if (in_array($attribute->getAttributeCode(), $extraProductAttributes) && !in_array( $attribute->getAttributeCode(), self::PRODUCT_ATTR_TO_EXCL )) {
                try {
                    $value = $attribute->getFrontend()->getValue($product);

                    if ($value instanceof Phrase) {
                        $value = (string)$value;
                    }

                    $requestBody['custom_data'][$attribute->getAttributeCode()] = !is_null($value) ? $value : "";
                } catch ( \Exception $e ) {
                    // Skip
                }
            }
        }

        return $requestBody;
    }

    /**
     * Map Order entity to request body array
     *
     * @param Order|OrderInterface $order
     * @return array
     */
    public function mapOrderToRequestBody( $order )
    {
        $requestBody = [
            "order_id" => $order->getId(),
            "email" => $order->getCustomerEmail(),
            "payment" => $order->getPayment()->getMethod(),
            "total" => $this->formatPriceForShm( $order->getGrandTotal() ),
            "product_ids"=> [],
            "order_date"=> date('Y-m-d\TH:i:s', strtotime($order->getCreatedAt())),
            "custom_data"=> [
                "company_name" => $order->getData('company'),
                "discount_used" => $order->getDiscountAmount(),
            ]
        ];

        $requestBody["key"] = $this->shoppingMindsConfig->getApiExportKey( $requestBody, ["payment", "total", "email"] );

        /** @var Order\Item $orderItem */
        foreach ($order->getAllItems() as $orderItem) {
            /** @var Product $product */
            $product = $orderItem->getProduct();

            $requestBody["product_ids"][] = $product->getId();
        }

        return $requestBody;
    }

    /**
     * Map Customer entity to request body array
     *
     * @param Customer|CustomerInterface $customer
     * @return array
     */
    public function mapCustomerToRequestBody( $customer )
    {
        # Address should be the last address that was added by the customer.
        /** @var \Magento\Customer\Api\Data\AddressInterface[] $customerAddresses */
        $customerAddresses = $customer->getAddresses();
        $addressIndexToUse = count( $customerAddresses ) - 1;

        /** @var \Magento\Customer\Api\Data\AddressInterface $customerAddress */
        $customerAddress = isset( $customerAddresses[$addressIndexToUse] ) ? $customerAddresses[$addressIndexToUse] : false;

        if( $customerAddress ) {
            $streetWithHouseNumber = $customerAddress->getStreet();
            $streetWithHouseNumber = $this->splitStreetAndHouseNumber(
                $streetWithHouseNumber[0],
                isset($streetWithHouseNumber[1]) ? $streetWithHouseNumber[1] : null
            );
        } else {
            $streetWithHouseNumber['number'] = "";
            $streetWithHouseNumber['street'] = "";
        }

        $requestBody = [
            "customer_id" => $customer->getId(),
            "email"=> $customer->getEmail(),
            "birth_date" => $customer->getDob(),
            "first_name" => $customer->getFirstname(),
            "gender" => substr($customer->getGender(), 0, 1),
            "last_name" => $customer->getLastname(),
            "city" => $customerAddress ? $customerAddress->getCity() : "",
            "country" => $customerAddress ? $customerAddress->getCountryId() : "",
            "house_number" => $streetWithHouseNumber['number'],
            "street" => $streetWithHouseNumber['street'],
            "zip_code" => $customerAddress ? $customerAddress->getPostcode() : "",
            "custom_data"=> [
                "name_prefix" => $customer->getPrefix(),
                "name_suffix" => $customer->getSuffix(),
                "middle_name" => $customer->getMiddlename(),
                'address_company' => $customerAddress ? $customerAddress->getCompany() : "",
                'created_on' => strtotime( $customer->getCreatedAt() )
            ]
        ];

        $requestBody["key"] = $this->shoppingMindsConfig->getApiExportKey( $requestBody, ["email"] );

        return $requestBody;
    }


    /**
     * "Filters" are attributes marked as filterable.
     * An simple 1d array of attribute_codes should be provided here.
     *
     * @param array $filters
     * @return array
     */
    public function mapFiltersToRequestBody( array $filters )
    {
        $requestBody = [
            "filters" => $filters
        ];

        # Shopping Minds wants to receive the filters in alphabetic order.
        sort($requestBody["filters"]);

        $requestBody["key"] = $this->shoppingMindsConfig->getApiExportKey( ["filters" => join("", $requestBody["filters"])], ["filters"] );

        return $requestBody;
    }

    /**
     * Helper function to get an array of simple category path strings of the given product.
     *
     * @param Product $product
     *
     * @return array|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getCategoryPathsForProduct( Product $product )
    {
        $categories = [];
        foreach ($product->getCategoryCollection() as $category) {
            $category = $this->categoryRepository->get($category->getId());

            $categoryPath = '';
            foreach ($category->getParentCategories($category) as $parentCategory) {
                $categoryPath .= '/' . $parentCategory->getName();
            }

            if(!empty($categoryPath)){
                $categories[] .= $categoryPath;
            }
        }

        return !empty($categories) ? $categories : null;
    }

    /**
     * Shopping Minds needs the price following a specific format.
     *
     * @param $price
     *
     * @return int
     */
    private function formatPriceForShm( $price )
    {
        $price = str_replace($this->priceCurrency->getCurrencySymbol(), '', $this->priceCurrency->convertAndFormat($price) );
        $price = str_replace('.', '', $price );

        return (int)filter_var($price,FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Magento 2 doesn't provide a separate Street and house number, while Shopping Minds wants to get these values mapped to specific keys.
     * This function tries to give back the street and house number mapped to an array.
     *
     * @param $address
     * @param null $houseNumber
     *
     * @return array
     */
    private function splitStreetAndHouseNumber( $address, $houseNumber = null )
    {
        if( !is_null($houseNumber) ) {

            if ( preg_match('/^(?P<street>\d*\D+[^A-Z]) (?P<number>[^a-z]?\D*\d+.*)$/', $address, $result) )
            {
                // $result[1] will have the steet name
                $streetName = $result[1];
                // and $result[2] is the number part.
                $streetNumber = $result[2];

                $streetWithHouseNumber = [
                    "street" => $streetName,
                    "number" => $streetNumber
                ];
            } else {
                $streetWithHouseNumber =  [
                    "street" => $address,
                    "number" => $address
                ];
            }

        } else {
            $streetWithHouseNumber =  [
                "street" => $address,
                "number" => $houseNumber
            ];
        }

        return $streetWithHouseNumber;
    }
}