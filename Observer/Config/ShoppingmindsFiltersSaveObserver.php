<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Observer\Config;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;

use Shoppingminds\Base\Model\Shoppingminds;
use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;

/**
 * Observes the admin_system_config_changed_section_shm_filters event to export selected filters to the Shopping Minds API Endpoint
 *
 * Class ShoppingmindsFiltersSaveObserver
 *
 * @package Shoppingminds\Base\Observer\Config
 */
class ShoppingmindsFiltersSaveObserver implements ObserverInterface
{
    /** @var ManagerInterface  */
    protected $messageManager;

    /** @var Shoppingminds  */
    protected $shoppingminds;
    /** @var ShoppingmindsConfig  */
    protected $shoppingmindsConfig;

    /**
     * ShoppingmindsFiltersSaveObserver constructor.
     *
     * @param Shoppingminds $shoppingminds
     * @param ManagerInterface $messageManager
     * @param ShoppingmindsConfig $shoppingmindsConfig
     */
    public function __construct(
        Shoppingminds $shoppingminds,
        ManagerInterface $messageManager,
        ShoppingmindsConfig $shoppingmindsConfig
    )
    {
        $this->shoppingminds = $shoppingminds;
        $this->messageManager = $messageManager;
        $this->shoppingmindsConfig = $shoppingmindsConfig;

    }

    /**
     * Gets executed on the admin_system_config_changed_section_shm_filters event
     *
     * @param Observer $observer
     */
    public function execute( Observer $observer )
    {
        $attributes = null;
        $data = $observer->getEvent()->getData();

        foreach ( $data['changed_paths'] as $setting )
        {
            if( $setting == ShoppingmindsConfig::SHM_FILTER_ATTRIBUTES ) {
                $attributes = $this->shoppingmindsConfig->getProductFilterAttributes();
            }
        }

        if ( $attributes != null && count($attributes) > 0 )
        {
            $this->shoppingminds->exportFiltersToShm( $attributes);
        }
    }

}