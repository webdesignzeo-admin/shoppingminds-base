<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Observer\Config;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;

use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;

/**
 * Observes the admin_system_config_changed_section_shm_general event to check if the Shopping Minds script was provided
 *
 * Class ShoppingmindsGeneralSaveObserver
 *
 * @package Shoppingminds\Base\Observer\Config
 */
class ShoppingmindsGeneralSaveObserver implements ObserverInterface
{
    protected $messageManager;

    /** @var ShoppingmindsConfig  */
    protected $shoppingMindsConfig;

    /**
     * ShoppingmindsGeneralSaveObserver constructor.
     *
     * @param ManagerInterface $messageManager
     * @param ShoppingmindsConfig $shoppingMindsConfig
     */
    public function __construct(
        ManagerInterface $messageManager,
        ShoppingmindsConfig $shoppingMindsConfig
    )
    {
        $this->messageManager = $messageManager;
        $this->shoppingMindsConfig = $shoppingMindsConfig;
    }

    /**
     * Gets executed on the admin_system_config_changed_section_shm_general event
     *
     * @param Observer $observer
     */
    public function execute( Observer $observer )
    {
       try {
           # Try to get the head script.
           $this->shoppingMindsConfig->getHeadScript();
       } catch (  \Magento\Framework\Exception\LocalizedException $e ) {
           # If the head script was not provided we show an error message to alert the user of the requirement.
           $this->messageManager->addErrorMessage( __('You forgot to add the HEAD script under Shopping Minds > General > HEAD Settings!') );
       }
    }

}