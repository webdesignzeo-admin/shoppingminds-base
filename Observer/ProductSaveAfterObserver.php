<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Catalog\Model\Product;


use Shoppingminds\Base\Model\Http\Api\Request;
use Shoppingminds\Base\Model\Http\Api\RequestFactory;
use Shoppingminds\Base\Model\Http\Api\Response;
use Shoppingminds\Base\Model\Shoppingminds;
use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;
use Shoppingminds\Base\Helper\ShoppingMindsAdapter;

/**
 * Observes the catalog_product_save_after event to keep the product updated on the side of Shopping Minds
 *
 * Class ProductSaveAfterObserver
 *
 * @package Shoppingminds\Base\Observer
 */
class ProductSaveAfterObserver implements ObserverInterface
{
    /** @var ShoppingmindsConfig  */
    protected $config;
    /** @var RequestFactory  */
    protected $requestFactory;

    /** @var ShoppingMindsAdapter  */
    protected $shoppingMindsAdapter;

    /**
     * SaveProductAfterObserver constructor.
     *
     * @param ShoppingmindsConfig $config
     * @param RequestFactory $requestFactory
     * @param ShoppingMindsAdapter $shoppingMindsAdapter
     */
    public function __construct(
        ShoppingmindsConfig $config,
        RequestFactory $requestFactory,
        ShoppingMindsAdapter $shoppingMindsAdapter
    )
    {
        $this->config = $config;
        $this->requestFactory = $requestFactory;
        $this->shoppingMindsAdapter = $shoppingMindsAdapter;
    }

    /**
     * Observes the catalog_product_save_after event
     *
     * @param Observer $observer
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute( Observer $observer )
    {
        if( $this->config->isProductExportEnabled() ) {
            /** @var Product $product */
            $product = $observer->getEvent()->getProduct();

            $requestBody = $this->shoppingMindsAdapter->mapProductToRequestBody( $product );

            /** @var Request $request */
            $request = $this->requestFactory->create();

            /** @var Response $response */
            $request->post( Shoppingminds::SHM_PRODUCT_ENDPOINT, $requestBody );
        }
    }
}