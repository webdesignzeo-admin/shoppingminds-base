<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;

use Shoppingminds\Base\Model\Shoppingminds;
use Shoppingminds\Base\Model\Http\Api\Request;
use Shoppingminds\Base\Model\Http\Api\RequestFactory;
use Shoppingminds\Base\Model\Http\Api\Response;
use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;

use Shoppingminds\Base\Helper\ShoppingMindsAdapter;

/**
 * Observes the sales_order_save_after event to keep the customer updated on the side of Shopping Minds
 *
 * Class OrderSaveAfterObserver
 *
 * @package Shoppingminds\Base\Observer
 */
class OrderSaveAfterObserver implements ObserverInterface
{
    /** @var ShoppingmindsConfig  */
    protected $config;
    /** @var RequestFactory  */
    protected $requestFactory;

    /** @var ShoppingMindsAdapter  */
    protected $shoppingMindsAdapter;

    /**
     * SaveProductAfterObserver constructor.
     *
     * @param ShoppingmindsConfig $config
     * @param RequestFactory $requestFactory
     * @param ShoppingMindsAdapter $shoppingMindsAdapter
     */
    public function __construct(
        ShoppingmindsConfig $config,
        RequestFactory $requestFactory,
        ShoppingMindsAdapter $shoppingMindsAdapter
    )
    {
        $this->config = $config;
        $this->requestFactory = $requestFactory;
        $this->shoppingMindsAdapter = $shoppingMindsAdapter;
    }

    /**
     * Observes the sales_order_save_after event
     *
     * @param Observer $observer
     */
	public function execute( Observer $observer )
	{
        if( $this->config->isOrderExportEnabled() ) {
            /** @var Order $order */
            $order = $observer->getEvent()->getOrder();

            $requestBody = $this->shoppingMindsAdapter->mapOrderToRequestBody( $order );

            /** @var Request $request */
            $request = $this->requestFactory->create();

            /** @var Response $response */
            $request->post( Shoppingminds::SHM_ORDER_ENDPOINT, $requestBody );
        }
	}
}