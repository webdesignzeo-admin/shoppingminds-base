<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;

class HeadBlock extends Template {
    /** @var ShoppingmindsConfig  */
    protected $shoppingMindsConfig;

    /**
     * HeadBlock constructor.
     *
     * @param ShoppingmindsConfig $shoppingMindsConfig
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        ShoppingmindsConfig $shoppingMindsConfig,
        Context $context,
        $data = []
    )
    {
        $this->shoppingMindsConfig = $shoppingMindsConfig;

        parent::__construct( $context, $data );
    }

    /**
     * Returns content of Shopping Minds @var \Shoppingminds\Base\Model\Config\ShoppingmindsConfig::SHM_HEAD_SCRIPT config field
     *
     * @return string|null
     */
    public function getHeadScripts()
    {
        if( $this->shoppingMindsConfig->shouldAddHeadScript() ) {
            try {
                return $this->shoppingMindsConfig->getHeadScript();
            } catch ( \Magento\Framework\Exception\LocalizedException $e ) {
                return "<script>// Shopping Minds script is missing.</script>";
            }
        }

        return null;
    }
}