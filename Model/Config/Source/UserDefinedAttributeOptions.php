<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Model\Config\Source;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;

/**
 * Simple class rendering the attribute multi select for use in the Shopping Minds Product Export configuration
 *
 * Class UserDefinedAttributeOptions
 *
 * @package Shoppingminds\Base\Model\Config\Source
 */
class UserDefinedAttributeOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * SortOrder builder
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * UserDefinedAttributeOptions constructor.
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param AttributeRepositoryInterface $attributeRepository
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        AttributeRepositoryInterface $attributeRepository,
        SortOrderBuilder $sortOrderBuilder
    ){
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->attributeRepository = $attributeRepository;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    public function toOptionArray()
    {
        // Add sorting on the attribute frontend label
        $sortOrder = $this->sortOrderBuilder
                           ->setField('frontend_label')
                           ->setDirection(SortOrder::SORT_ASC)
                           ->create();

        // Add filter to get only the user defined attributes
        $searchCriteria = $this->searchCriteriaBuilder
//                               ->addFilter('is_user_defined', 1)
                               ->addSortOrder($sortOrder)
                               ->create();

        // Search for product attributs only with the defined filter
        $attributeRepository = $this->attributeRepository->getList(
            ProductAttributeInterface::ENTITY_TYPE_CODE,
            $searchCriteria
        );

        // Convert the results to a option array
        $userDefinedAttributeOptions = [];
        foreach ($attributeRepository->getItems() as $items) {
            $userDefinedAttributeOptions[] = [
                'value' => $items->getAttributeCode(),
                'label' => $items->getFrontendLabel()
            ];
        }

        return $userDefinedAttributeOptions;
    }
}