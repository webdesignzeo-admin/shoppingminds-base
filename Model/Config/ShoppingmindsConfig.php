<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Model\Config;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Information as StoreInformation;
use Magento\Store\Model\Store;

/**
 * The base Shopping Minds Config.
 * This class is extended by the individual Shopping Minds modules and provides helper functions to retrieve the Shopping Minds configuration values.
 *
 * Class ShoppingmindsConfig
 *
 * @package Shoppingminds\Base\Model\Config
 */
class ShoppingmindsConfig {
	const SHM_GENERAL_ENABLED = 'shm_general/general_settings/enabled';
	const SHM_TEST_IPS = 'shm_general/general_settings/test_ips';
	const SHM_HEAD_ADD_SCRIPT = 'shm_general/head_settings/add_script';
	const SHM_HEAD_SCRIPT = 'shm_general/head_settings/script';
	const SHM_API_URL = 'shm_general/api_config/api_url';
	const SHM_PRIVATE_KEYWORD = 'shm_general/api_config/private_keyword';
	const SHM_EXTRA_PRODUCT_ATTRIBUTES = 'shm_products/general_settings/attributes';
	const SHM_FILTER_ATTRIBUTES = 'shm_filters/general_settings/attributes';
    const SHM_PRODUCT_EXPORT_ENABLED = 'shm_orders/general_settings/enabled';
    const SHM_CUSTOMER_EXPORT_ENABLED = 'shm_customers/general_settings/enabled';
    const SHM_ORDER_EXPORT_ENABLED = 'shm_products/general_settings/enabled';


    const SHM_GENERAL_RECOMMENDATIONS_LANG = \Magento\Config\Model\Config\Backend\Admin\Custom::XML_PATH_GENERAL_LOCALE_CODE;
    const SHM_GENERAL_RECOMMENDATIONS_COUNTRY = \Magento\Config\Model\Config\Backend\Admin\Custom::XML_PATH_GENERAL_COUNTRY_DEFAULT;

	/** @var ScopeConfigInterface  */
	protected $scopeConfig;

	/** @var  Store */
	protected $store;

	/** @var \Magento\Framework\DataObject  */
	protected $storeInfo;

	/**
	 * Config constructor.
	 *
	 * @param ScopeConfigInterface $scopeConfig
	 * @param StoreInformation     $storeInfo
	 * @param Store                $store
	 */
	public function __construct(
		ScopeConfigInterface $scopeConfig,
		StoreInformation $storeInfo,
		Store $store
	){
		$this->scopeConfig = $scopeConfig;
		$this->store = $store;
		$this->storeInfo = $storeInfo->getStoreInformationObject( $store );
	}

    /**
     * Helper function to generate the API export key for each request body
     *
     * @param array $exportRequestBody
     * @param array $publicFields eg. ["product_url", "name", "price"] || ["payment", "total", "email"]
     *
     * @return string
     */
    public function getApiExportKey( array $exportRequestBody, array $publicFields)
    {
        sort($publicFields);

        $joinedValues = '';
        foreach($publicFields as $publicField){
            $joinedValues .= (string) $exportRequestBody[$publicField];
        }
        $fullValue = $joinedValues . $this->getPrivateKeyword();

        $encoded_value = utf8_encode($fullValue);

        return hash('sha256', $encoded_value);
    }

    /**
     * --------------------------------------
     * CONFIGURATION HELPER FUNCTIONS
     * --------------------------------------
     */

    /**
     * Check if the module is enabled or test mode is activated
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->isTestMode() || $this->scopeConfig->isSetFlag( self::SHM_GENERAL_ENABLED, ScopeInterface::SCOPE_STORE );
    }

    /**
     * Returns true if the sm_test query param is included in the url with the value "1"
     * Method is called in various functions checking if functionality is enabled and will force enable various functionality.
     *
     * @return bool
     */
    public function isTestMode()
    {
        return (isset($_GET['sm_test']) && $_GET['sm_test'] == '1') || (isset($_POST['sm_test']) && $_POST['sm_test'] == '1');
    }

	public function getApiUrl( $endpoint )
	{
		$apiBaseUrl = $this->getApiBaseUrl();

		$apiBaseUrl = str_replace( '{country}', $this->getStoreFrontCountry(), $apiBaseUrl );
		$apiBaseUrl = str_replace( '{lang}', $this->getStoreFrontLanguage(), $apiBaseUrl );

		$apiUrl = ( substr($apiBaseUrl, -1) == '/' ) ? $apiBaseUrl.$endpoint : "$apiBaseUrl/$endpoint";

		return $apiUrl;
	}

    /**
     * The module can't function without the base api url.
     * This needs to be provided by the customer via the SHM_API_URL configuration field.
     * @return mixed
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function getApiBaseUrl()
	{
		return $this->getRequiredValue( self::SHM_API_URL );
	}

    /**
     * Extra product attributes to include in the Product Export
     * @see \Shoppingminds\Base\Helper\ShoppingMindsAdapter
     *
     * @return array
     */
	public function getExtraProductAttributes() {
        $value = $this->scopeConfig->getValue( self::SHM_EXTRA_PRODUCT_ATTRIBUTES, ScopeInterface::SCOPE_STORE );
        $values = explode(',', $value);
        if($values[0] != '') {
            return $values;
        } else {
            return array();
        }
    }

    /**
     * Product filter attributes to export to Shopping Minds
     * According to requirements: By default all should be selected.
     * @see \Shoppingminds\Base\Helper\ShoppingMindsAdapter
     *
     * @return array
     */
	public function getProductFilterAttributes() {
        $value = $this->scopeConfig->getValue( self::SHM_FILTER_ATTRIBUTES, ScopeInterface::SCOPE_STORE );
        $values = explode(',', $value);
        if($values[0] != '') {
            return $values;
        } else {
            return array();
        }
    }

    /**
     * The private key is required to talk to the Shopping Minds API Endpoint
     *
     * @return mixed
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPrivateKeyword()
    {
        return $this->getRequiredValue( self::SHM_PRIVATE_KEYWORD );
    }

    /**
     * If the SHM_HEAD_ADD_SCRIPT is set to "yes" a script should be provided, if this is not the case an exception will be thrown which will show a notification in the admin.
     * else null is returned
     *
     * @return string|null
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getHeadScript()
    {
        if( $this->shouldAddHeadScript() ) {
            return $this->getRequiredValue( self::SHM_HEAD_SCRIPT, ScopeInterface::SCOPE_STORE );
        } else {
            return null;
        }
    }

    public function shouldAddHeadScript()
    {
        return $this->scopeConfig->isSetFlag( self::SHM_HEAD_ADD_SCRIPT, ScopeInterface::SCOPE_STORE );
    }

    public function isProductExportEnabled()
    {
        return $this->isEnabled() ? $this->isTestMode() || $this->scopeConfig->isSetFlag( self::SHM_PRODUCT_EXPORT_ENABLED, ScopeInterface::SCOPE_STORE ) : false;
    }

    public function isOrderExportEnabled()
    {
        return $this->isEnabled() ? $this->isTestMode() || $this->scopeConfig->isSetFlag( self::SHM_ORDER_EXPORT_ENABLED, ScopeInterface::SCOPE_STORE ) : false;
    }

    public function isCustomerExportEnabled()
    {
        return $this->isEnabled() ? $this->isTestMode() || $this->scopeConfig->isSetFlag( self::SHM_CUSTOMER_EXPORT_ENABLED, ScopeInterface::SCOPE_STORE ) : false;
    }

    /**
     * Retrieve the store front language code
     *
     * @return mixed
     */
    public function getStoreFrontLanguage()
    {
        $lang = $this->scopeConfig->getValue( self::SHM_GENERAL_RECOMMENDATIONS_LANG, ScopeInterface::SCOPE_STORE );
        $lang = substr($lang, 0, strpos($lang, '_'));

        return $lang;
    }

    /**
     * Retrieve the store front crountry
     *
     * @return mixed
     */
    public function getStoreFrontCountry()
    {
        $country = $this->scopeConfig->getValue( self::SHM_GENERAL_RECOMMENDATIONS_COUNTRY, ScopeInterface::SCOPE_STORE );
        $country = strtolower($country);

        return $country;
    }

    /**
     * Use this function if a configuration value is required for the module to function correctly.
     * It will throw an LocalizedException which will be turned into an notification in the admin, if the value is not provided.
     *
     * @param $systemFieldPath
     *
     * @return mixed
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getRequiredValue( $systemFieldPath )
	{
		$value = $this->scopeConfig->getValue( $systemFieldPath, ScopeInterface::SCOPE_STORE );

		if( is_null($value) ) {
			throw new \Magento\Framework\Exception\LocalizedException( __($systemFieldPath . ' is required.') );
		}

		return $value;
	}
}