<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Model;

use Shoppingminds\Base\Model\Export\Queue;
use Shoppingminds\Base\Model\Export\QueueFactory;
use Shoppingminds\Base\Model\Http\Api\Request;
use Shoppingminds\Base\Model\Http\Api\RequestFactory;
use Shoppingminds\Base\Helper\ShoppingMindsAdapter;

/**
 * Base Shopping Mind model responsible of core functionality like scheduling and exporting entities to Shopping Minds
 *
 * Class Shoppingminds
 *
 * @package Shoppingminds\Base\Model
 */
class Shoppingminds {

    /**
     * Export Types defined by Shopping Minds.
     * These are used as an identifier in the Export Queue.
     */
    const SHM_PRODUCT_ENDPOINT = 'import_feed';
    const SHM_ORDER_ENDPOINT = 'order';
    const SHM_CUSTOMER_ENDPOINT = 'visitor';
    const SHM_FILTER_ENDPOINT = 'filters';

    protected $requestFactory;
    protected $shoppingMindsAdapter;
    protected $_queueFactory;

    /**
     * Shoppingminds constructor.
     *
     * @param RequestFactory $requestFactory
     * @param ShoppingMindsAdapter $shoppingMindsAdapter
     * @param Queue $queue
     */
    public function __construct(
        RequestFactory $requestFactory,
        ShoppingMindsAdapter $shoppingMindsAdapter,
        QueueFactory $queueFactory
    )
    {
        $this->requestFactory = $requestFactory;
        $this->shoppingMindsAdapter = $shoppingMindsAdapter;
        $this->_queueFactory = $queueFactory;
    }

    /**
     * Schedule a collection of entities for export to a specific endpoint (provided as export type)
     *
     * @param $entityCollection
     * @param $exportType
     *
     * @return array
     *
     * @throws \Exception
     */
    public function scheduleEntitiesForProcessing( $entityCollection, $exportType )
    {
        $results = [
            'added' => 0,
            'rejected' => 0
        ];

        foreach( $entityCollection as $entity ) {
            /** @var Queue $queue */
            $queue = $this->_queueFactory->create();
            $queue->setEntityId($entity->getId());
            $queue->setExportType($exportType); # magic method filling the export_type column

            try {
                $queue->save();
                $results['added']++;
            } catch ( \Magento\Framework\Exception\AlreadyExistsException $e ) {
                $results['rejected']++;
            }
        }

        return $results;
    }

    /**
     * Export a queue item to Shopping Minds using the Queue Model
     *
     * @param Queue $queueItem
     *
     * @return Http\Api\Response|string
     */
    public function exportToShm( Queue $queueItem )
    {
        $response = $queueItem->process();

        return $response;
    }

    /**
     * Separate function to export the filters to Shopping Minds
     * These don't use the queue functionality like most complex entities
     * @param $filters
     *
     * @return Http\Api\Response|string
     */
    public function exportFiltersToShm( $filters )
    {
        $requestBody = $this->shoppingMindsAdapter->mapFiltersToRequestBody( $filters );

        try {
            /** @var Request $request */
            $request = $this->requestFactory->create();
            $response = $request->post( self::SHM_FILTER_ENDPOINT, $requestBody );
        } catch ( \Exception $e ) {
            $response = $e->getMessage();
        }

        return $response;
    }
}