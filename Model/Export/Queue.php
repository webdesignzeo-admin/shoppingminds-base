<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Model\Export;

use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Model\AbstractModel;

use Magento\Sales\Model\OrderRepository;
use Shoppingminds\Base\Model\Http\Api\Request;
use Shoppingminds\Base\Model\Http\Api\RequestFactory;
use Shoppingminds\Base\Helper\ShoppingMindsAdapter;
use Shoppingminds\Base\Model\Http\Api\Response;
use Shoppingminds\Base\Model\Shoppingminds;

/**
 * Queue model responsible for processing the export queue.
 * This method is mainly invoked by the cron and on demand via CLI commands
 * @see \Shoppingminds\Base\Cron\*
 * @see \Shoppingminds\Base\Console\Command\*
 *
 * Class Queue
 *
 * @package Shoppingminds\Base\Model\Export
 */
class Queue extends AbstractModel {
    const CACHE_TAG = 'shoppingminds_base_export_queue';

    protected $_cacheTag = self::CACHE_TAG;

    protected $_eventPrefix = self::CACHE_TAG;

    /** @var ShoppingMindsAdapter */
    protected $_shoppingMindsAdapter;
    /** @var RequestFactory */
    protected $_requestFactory;
    /** @var ProductRepository */
    protected $_productRepository;
    /** @var OrderRepository */
    protected $_orderRepository;
    /** @var CustomerRepository */
    protected $_customerRepository;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ShoppingMindsAdapter $shoppingMindsAdapter,
        RequestFactory $requestFactory,
        ProductRepository $productRepository,
        OrderRepository $orderRepository,
        CustomerRepository $customerRepository,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        # Register the resource model representing the shoppingminds_export_queue table
        $this->_init('Shoppingminds\Base\Model\ResourceModel\Export\Queue');
     
        $this->_requestFactory = $requestFactory;
        $this->_shoppingMindsAdapter = $shoppingMindsAdapter;
        $this->_productRepository = $productRepository;
        $this->_orderRepository = $orderRepository;
        $this->_customerRepository = $customerRepository;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function process()
    {
        $response = "";

        $entity_id = $this->getData('entity_id');
        $export_type = $this->getData('export_type');

        try {
            # Retrieve the entity or the provided entity_id and map it to a Shopping Minds request body
            switch ( $export_type ) {
                case Shoppingminds::SHM_PRODUCT_ENDPOINT:
                    $entity = $this->_productRepository->getById( $entity_id );
                    $requestBody = $this->_shoppingMindsAdapter->mapProductToRequestBody( $entity );
                    break;
                case Shoppingminds::SHM_ORDER_ENDPOINT:
                    $entity = $this->_orderRepository->get( $entity_id );
                    $requestBody = $this->_shoppingMindsAdapter->mapOrderToRequestBody( $entity );
                    break;
                case Shoppingminds::SHM_CUSTOMER_ENDPOINT:
                    $entity = $this->_customerRepository->getById( $entity_id );
                    $requestBody = $this->_shoppingMindsAdapter->mapCustomerToRequestBody( $entity );
                    break;
                # We throw and invalid argument exception for future development of extra endpoints. Don't catch this..
                default: throw new \InvalidArgumentException("Export type is not supported."); break;
            }

            try {
                /** @var Request $request */
                $request = $this->_requestFactory->create();
                /** @var Response $response */
                $response = $request->post( $export_type, $requestBody );
                unset($request);

                if( $response->isSuccess() ) $this->delete();
            } catch ( \Exception $e ) {
                $response = $e->getMessage() . " for request body " . json_encode( $response->getRequest()->getRequestBody() );
            }

        } catch ( \Magento\Framework\Exception\NoSuchEntityException $e ) {
            # entity does not exist anymore delete queue row
            $this->delete();
        }

        return $response;
    }
}