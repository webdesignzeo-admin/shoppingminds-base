<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Model\Http\Api;

use Shoppingminds\Base\Model\Config\ShoppingmindsConfig;
use Shoppingminds\Base\Model\Http\RequestFactory;
use Shoppingminds\Base\Model\Shoppingminds;
use Zend\Http\Exception\InvalidArgumentException;

/**
 * Shopping Minds Zend request wrapper.
 * Simplifies the Zend Request with a single post function and performs some validation.
 *
 * Class Request
 *
 * @package Shoppingminds\Base\Model\Http\Api
 */
class Request {

    const VALID_ENDPOINTS = [
        Shoppingminds::SHM_PRODUCT_ENDPOINT,
        Shoppingminds::SHM_FILTER_ENDPOINT,
        Shoppingminds::SHM_ORDER_ENDPOINT,
        Shoppingminds::SHM_CUSTOMER_ENDPOINT,
    ];

    /**
     * Required Customer request body fields specified in the original Shopping Minds Documentation
     * @var array
     */
    const REQUIRED_VISITOR_FIELDS = [
        "email",
        "key",
        "birth_date" ,
        "first_name" ,
        "gender" ,
        "last_name" ,
        "city" ,
        "country" ,
        "house_number" ,
        "street" ,
        "zip_code" ,
    ];

    /**
     * Required Order request body fields specified in the original Shopping Minds Documentation
     * @var array
     */
    const REQUIRED_ORDER_FIELDS = [
        "email",
        "payment",
        "total",
        "product_ids",
        "order_date"
    ];

    /**
     * Required Product request body fields specified in the original Shopping Minds Documentation
     * @var array
     */
    const REQUIRED_PRODUCT_FIELDS = [
        "product_id",
        "name",
        "price",
        "product_url",
        "image_url"
    ];

    /**
     * Required Filter request body fields specified in the original Shopping Minds Documentation
     * @var array
     */
    const REQUIRED_FILTER_FIELDS = [
        "filters",
    ];

    protected $_config;
    protected $_requestFactory;

    protected $_url;
    protected $_requestBody;

    public function __construct(
        ShoppingmindsConfig $config,
        RequestFactory $requestFactory
    )
    {
        $this->_config = $config;
        $this->_requestFactory = $requestFactory;
    }

    public function post( $endpoint, $requestBody )
    {
        $this->setUrl( $endpoint );
        $this->setRequestBody( $requestBody );

        if( $this->requestBodyIsValid( $endpoint, $requestBody ) ) {
            /** @var \Shoppingminds\Base\Model\Http\Request $request */
            $request = $this->_requestFactory->create();
            $request->init( $this->getUrl() );

            $response = $request->post( $this->getRequestBody() );

            $apiResponse = new Response( $response );
            $apiResponse->setRequest( $this ); # add the request to the response for extra debug information
        } else {
            throw new InvalidArgumentException(__("Provided request body does not meet requirements for endpoint %1.", $endpoint));
        }

        return $apiResponse;
    }

    public function getUrl()
    {
        return $this->_url;
    }

    public function getRequestBody()
    {
        return $this->_requestBody;
    }

    protected function setRequestBody( $requestBody )
    {
        $this->_requestBody = $requestBody;
    }

    protected function setUrl( $endpoint )
    {
        if( $this->isValidEndpoint( $endpoint ) )
            $this->_url = $this->_config->getApiUrl( $endpoint );
        else throw new InvalidArgumentException("Given endpoint $endpoint is not a supported API endpoint.");
    }

    private function isValidEndpoint( $endpoint ) {
        return in_array( $endpoint, self::VALID_ENDPOINTS );
    }

    private function requestBodyIsValid( $endpoint, $requestBody ) {
        $failed = false;

        switch ( $endpoint ) {
            case Shoppingminds::SHM_CUSTOMER_ENDPOINT:
                foreach (self::REQUIRED_VISITOR_FIELDS as $REQUIRED_VISITOR_FIELD)
                    $failed = !key_exists($REQUIRED_VISITOR_FIELD, $requestBody);
                break;
            case Shoppingminds::SHM_ORDER_ENDPOINT:
                foreach (self::REQUIRED_ORDER_FIELDS as $REQUIRED_ORDER_FIELD)
                    $failed = !key_exists($REQUIRED_ORDER_FIELD, $requestBody);
                break;
            case Shoppingminds::SHM_PRODUCT_ENDPOINT:
                foreach (self::REQUIRED_PRODUCT_FIELDS as $REQUIRED_PRODUCT_FIELD)
                    $failed = !key_exists($REQUIRED_PRODUCT_FIELD, $requestBody);
                break;
            case Shoppingminds::SHM_FILTER_ENDPOINT:
                foreach (self::REQUIRED_FILTER_FIELDS as $REQUIRED_FILTER_FIELD)
                    $failed = !key_exists($REQUIRED_FILTER_FIELD, $requestBody);
                break;
            default:
                throw new \InvalidArgumentException("The provided endpoint $endpoint is not supported.");
                break;
        }

        return !$failed;
    }
}