<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Model\Http\Api;

/**
 * Response class returned by the @see \Shoppingminds\Base\Model\Http\Api\Request
 *
 * Class Response
 *
 * @package Shoppingminds\Base\Model\Http\Api
 */
class Response {

    protected $_rawResponse;
    protected $_success;
    protected $_statusCode;
    protected $_message;
    protected $_content;
    /** @var Request */
    protected $_request;

    public function __construct(
        \Zend\Http\Response $response
    )
    {
        $this->setRawResponse( $response );
        $this->setSuccess( $response );
        $this->setStatusCode( $response );
        $this->setContent( $response );
        $this->setMessage( $response );
    }

    public function isSuccess()
    {
        return $this->_success;
    }

    protected function setSuccess( \Zend\Http\Response $response )
    {
        $this->_success = $this->responseWasSuccessful( $response );
    }

    public function getRawResponse()
    {
        return $this->_rawResponse;
    }

    protected function setRawResponse( \Zend\Http\Response $response )
    {
        $this->_rawResponse = $response;
    }

    protected function setStatusCode( \Zend\Http\Response $response )
    {
        $this->_statusCode = $response->getStatusCode();
    }

    public function getStatusCode()
    {
        return $this->_statusCode;
    }

    public function getContent()
    {
        return $this->_content;
    }

    protected function setContent( \Zend\Http\Response $response )
    {
        $this->_content = $response->getContent();
    }

    public function setRequest( Request $request )
    {
        $this->_request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->_request;
    }

    protected function setMessage( \Zend\Http\Response $response )
    {
        $content = json_decode($response->getContent(), true);
        $zendStatusMessage = isset( $content['status'] ) ? $content['status'] : false;

        if( $this->responseWasSuccessful( $response ) ) {
            if( $zendStatusMessage ) {
                $message = __('Success. %1', [ $zendStatusMessage ])->__toString();
            } else {
                $message = $response->getContent();
            }
        } else {
            if( $zendStatusMessage !== false ) {
                $message = __('Request rejected: %1. Request status code: %2', [$zendStatusMessage, $response->getStatusCode()])->__toString();
            } else {
                $message = __('An unexpected error occurred: Request status code: %1. URL: %2', [ $response->getStatusCode(). " - " .$response->getContent(), addslashes($this->getRequest()->getUrl()) ])->__toString();
            }
        }

        $this->_message = $message;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    private function responseWasSuccessful( \Zend\Http\Response $response )
    {
        return $response->getStatusCode() == 200;
    }
}