<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Shoppingminds\Base\Cron\ExportProductsCommand as ExportProductsCommandCron;

/**
 * Registers a command to make testing the product export cron easier
 * @see \Shoppingminds\Base\Cron\ExportProductsCommand for source
 *
 * Class ExportProductsCommand
 *
 * @package Shoppingminds\Base\Console\Command
 */
class ExportProductsCommand extends Command
{
    const COMMAND_NAME = 'shoppingminds:product:export';
    const COMMAND_DESC = 'Exports products to Shopping Minds.';

    /** @var ExportProductsCommandCron  */
    protected $exportProductsCommand;

    /**
     * ExportProductsCommand constructor.
     *
     * @param ExportProductsCommandCron $exportProductsCommand
     * @param null $name
     */
    public function __construct(
        ExportProductsCommandCron $exportProductsCommand,
        $name = null
    )
    {
        $this->exportProductsCommand = $exportProductsCommand;

        parent::__construct($name);
    }

    /**
     * Registers the command name with given description
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)->setDescription(self::COMMAND_DESC);
    }

    /**
     * Function gets executed with command "shoppingminds:product:export".
     * In turn calls the logic of the export products cron.
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->exportProductsCommand->execute();
    }
}