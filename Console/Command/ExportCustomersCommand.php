<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Shoppingminds\Base\Cron\ExportCustomersCommand as ExportCustomersCommandCron;

/**
 * Registers a command to make testing the customer export cron easier
 * @see \Shoppingminds\Base\Cron\ExportCustomersCommand for source
 *
 * Class ExportCustomersCommand
 *
 * @package Shoppingminds\Base\Console\Command
 */
class ExportCustomersCommand extends Command
{
    const COMMAND_NAME = 'shoppingminds:customer:export';
    const COMMAND_DESC = 'Exports customers to Shopping Minds.';

    /** @var ExportCustomersCommandCron */
    protected $exportCustomersCommand;

    /**
     * ExportOrdersCommand constructor.
     *
     * @param ExportCustomersCommandCron $exportCustomersCommand
     * @param null $name
     */
    public function __construct(
        ExportCustomersCommandCron $exportCustomersCommand,
        $name = null
    )
    {
        $this->exportCustomersCommand = $exportCustomersCommand;

        parent::__construct($name);
    }

    /**
     * Registers the command name with given description
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)->setDescription(self::COMMAND_DESC);
    }

    /**
     * Function gets executed with command "shoppingminds:customer:export".
     * In turn calls the logic of the export customers cron.
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $this->exportCustomersCommand->execute();
    }
}