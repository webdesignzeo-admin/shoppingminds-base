<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Shoppingminds\Base\Cron\ExportOrdersCommand as ExportOrdersCommandCron;

/**
 * Registers a command to make testing the order export cron easier
 * @see \Shoppingminds\Base\Cron\ExportOrdersCommand for source
 *
 * Class ExportOrdersCommand
 *
 * @package Shoppingminds\Base\Console\Command
 */
class ExportOrdersCommand extends Command
{
    const COMMAND_NAME = 'shoppingminds:order:export';
    const COMMAND_DESC = 'Exports orders to Shopping Minds.';

    protected $exportOrdersCommand;

    /**
     * ExportOrdersCommand constructor.
     *
     * @param ExportOrdersCommandCron $exportOrdersCommand
     * @param null $name
     */
    public function __construct(
        ExportOrdersCommandCron $exportOrdersCommand,
        $name = null
    )
    {
        $this->exportOrdersCommand = $exportOrdersCommand;

        parent::__construct($name);
    }

    /**
     * Registers the command name with given description
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)->setDescription(self::COMMAND_DESC);
    }

    /**
     * Function gets executed with command "shoppingminds:order:export".
     * In turn calls the logic of the export orders cron.
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->exportOrdersCommand->execute();
    }
}