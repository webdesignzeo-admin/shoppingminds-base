---------------------------------------------
     Scroll down to bottom for english.
---------------------------------------------

## Voordat je begint

  - Controleer de versie van je Magento 2 webshop, deze module ondersteunt de Magento 2.3.x versies. Het versienummer is rechtsonder in het admin paneel van een Magento 2 webshop te vinden.
  - Installeer de module altijd eerst op een test omgeving om eventuele conflicten met eerder geïnstalleerde modules te voorkomen.
  - Als de de module niet naar verwachting werkt of fouten optreden, adviseren wij om eerst te checken voor conflicten met andere modules in de shop. Gedurende de ontwikkeling hebben wij zoveel mogelijk rekeningen
    gehouden om conflicten te voorkomen, maar wij bieden geen gegarandeerde compatibliteit met andere modules.
    
### Licentie
Deze module is geschreven door [Zeo BV.](https://www.zeo.nl/) en wordt gratis uitgegeven aan de klanten van Shopping Minds BV. Alle rechten worden aan Shopping Minds BV. voorbehouden.


---------------------------------------------
# English version

## Before you start

  - Make sure your Magento Shop is version 2.3.x. You can find the version number at the bottom right of the Magento 2 admin panel.
  - It's highly advised to first install any Magento module in a test environment first. Conflicts can occur with other installed modules.
  - If the module doesn't function as expected, first check for conflicts with other modules in your shop. During the development we tried as much as possible to avoid possible conflicts with other modules, but
    we don't give of any garantee that conflicts will not occur.

### License
This module was written by [Zeo BV.](https://www.zeo.nl/) and is distributed for free to clients of Shopping Minds BV. All rights are reserved by Shopping Minds BV.

