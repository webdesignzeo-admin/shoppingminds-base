<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Cron;

use Magento\Framework\App\State;

use Shoppingminds\Base\Model\Http\Api\Response;
use Shoppingminds\Base\Model\Shoppingminds\Proxy as Shoppingminds;

use Shoppingminds\Base\Model\Export\Queue;
use Shoppingminds\Base\Model\Export\QueueFactory\Proxy as ShoppingMindsExportQueueFactory;

/**
 * Base class for entity export to Shopping Minds. Gets called via cron and optionally via CLI command.
 *
 * Class ExportCommand
 *
 * @package Shoppingminds\Base\Cron
 */
class ExportCommand
{
    const AREA_CODE = 'adminhtml'; # can be adminhtml or frontend
    const JOB_MAX_ERROR_NUM = 5; # Number of errors (HTTP status codes other than 200) allowed, before export breaks out.

    /** @var \Shoppingminds\Base\Model\Shoppingminds */
    protected $shoppingMinds;
    /** @var ShoppingMindsExportQueueFactory */
    protected $shoppingMindsExportQueueFactory;

    /**
     * ExportCommand constructor.
     *
     * @param State $state
     * @param Shoppingminds $shoppingMinds
     * @param ShoppingMindsExportQueueFactory $shoppingMindsExportQueueFactory
     */
    public function __construct(
        State $state,
        Shoppingminds $shoppingMinds,
        ShoppingMindsExportQueueFactory $shoppingMindsExportQueueFactory
    )
    {
        $this->setAreaCode($state);

        $this->shoppingMinds = $shoppingMinds;
        $this->shoppingMindsExportQueueFactory = $shoppingMindsExportQueueFactory;
    }

    /**
     * Performs the export of entities of given exportType to Shopping Minds
     * @see \Shoppingminds\Base\Model\Shoppingminds for supported export types defined by Shopping Minds.
     *
     * @param $exportType
     */
    public function run( $exportType )
    {
        # Create a queue model
        $queueListItems = $this->getQueueListItems( $exportType );

        # Define error counter to halt process in case of too many errors
        $errorCounter = 0;

        # Process the array of product entities by converting each one of them to a request body and sending it to the corresponding Shopping Minds endpoint
        /** @var Queue $queueListItem */
        foreach ($queueListItems as $queueListItem)
        {
            $response = $this->shoppingMinds->exportToShm( $queueListItem );

            if( $response->getStatusCode() !== 200 ) $errorCounter++;

            if( $response instanceof Response ) {
                echo $response->getMessage() . PHP_EOL;
            } else {
                echo $response . PHP_EOL;
            }

            if( $errorCounter >= self::JOB_MAX_ERROR_NUM ) {
                echo "Job halted. Too many errors. Num of errors: $errorCounter." . PHP_EOL; break;
            }
        }
    }

    /**
     * @param $exportType
     *
     * @return \Magento\Framework\DataObject[]
     */
    protected function getQueueListItems( $exportType )
    {
        /** @var Queue $queue */
        $queue = $this->shoppingMindsExportQueueFactory->create();

        # Retrieve all queue rows with entity ids from the queue for the given export type
        return $queue->getCollection()
                     ->addFieldToFilter('export_type', $exportType)
                     ->load()->getItems();
    }

    /**
     * Little helper function to prevent "Area code not set." error.
     * It sets the area code to the given area code. If it is already set the error is ignored.
     * @param $state
     */
    private function setAreaCode($state)
    {
        try {
            $state->setAreaCode(self::AREA_CODE);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            # ignore and continue..
        }
    }
}