<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Sales\Model\OrderRepository;
use Magento\Framework\Controller\ResultFactory;

use Shoppingminds\Base\Model\Shoppingminds;

/**
 * Controller to add Order entity ids to the scheduler queue for export.
 * @see \Shoppingminds\Base\Cron\ExportOrdersCommand for export code source
 *
 * Class Export
 *
 * @package Shoppingminds\Base\Controller\Adminhtml\Order
 */
class Export extends \Magento\Backend\App\Action
{
    protected $_publicActions = ['export'];

    /** @var OrderRepository  */
    protected $orderRepository;
    /** @var searchCriteriaInterface  */
    protected $searchCriteriaInterface;
    /** @var FilterBuilder  */
    protected $filterBuilder;
    /** @var FilterGroup  */
    protected $filterGroup;

    /** @var Shoppingminds  */
    protected $shoppingMinds;

    /**
     * Export constructor.
     *
     * @param Context $context
     * @param OrderRepository $orderRepository
     * @param searchCriteriaInterface $searchCriteriaInterface
     * @param FilterBuilder $filterBuilder
     * @param FilterGroup $filterGroup
     * @param Shoppingminds $shoppingMinds
     */
    public function __construct(
        Context $context,
        OrderRepository $orderRepository,
        searchCriteriaInterface $searchCriteriaInterface,
        FilterBuilder $filterBuilder,
        FilterGroup $filterGroup,
        Shoppingminds $shoppingMinds

    ) {
        $this->orderRepository = $orderRepository;

        $this->searchCriteriaInterface = $searchCriteriaInterface;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroup = $filterGroup;

        $this->shoppingMinds = $shoppingMinds;

        parent::__construct($context);
    }

    /**
     * Method called via route /{admin}/shoppingminds/order/export
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('entity_id')
                ->setConditionType('gt')
                ->setValue(0)
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);
        $orderList = $this->orderRepository->getList($this->searchCriteriaInterface);

        $results = $this->shoppingMinds->scheduleEntitiesForProcessing( $orderList, Shoppingminds::SHM_ORDER_ENDPOINT );

        if( $results['added'] > 0 ) {
            $this->messageManager->addSuccessMessage( $results['added']." orders are scheduled for export." );
        } else {
            $this->messageManager->addSuccessMessage( "All orders are already scheduled for export." );
        }

        if( $results['rejected'] > 0 ) {
            $this->messageManager->addErrorMessage( $results['rejected']." orders were skipped. With reason: Already in queue.");
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl($this->_redirect->getRefererUrl());
    }
}
