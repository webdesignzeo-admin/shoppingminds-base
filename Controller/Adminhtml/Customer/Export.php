<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Controller\Adminhtml\Customer;

use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\Framework\Controller\ResultFactory;

use Shoppingminds\Base\Model\Shoppingminds;

/**
 * Controller to add Customer entity ids to the scheduler queue for export.
 * @see \Shoppingminds\Base\Cron\ExportCustomersCommand for export code source
 *
 * Class Export
 *
 * @package Shoppingminds\Base\Controller\Adminhtml\Customer
 */
class Export extends \Magento\Backend\App\Action
{
    protected $_publicActions = ['export'];

    /** @var FilterGroup  */
    protected $filterGroup;
    /** @var FilterBuilder  */
    protected $filterBuilder;
    /** @var SearchCriteriaInterface  */
    protected $searchCriteriaInterface;
    /** @var CustomerRepository  */
    protected $customerRepository;

    /** @var Shoppingminds  */
    protected $shoppingMinds;

    /**
     * Export constructor.
     *
     * @param Context $context
     * @param SearchCriteriaInterface $searchCriteriaInterface
     * @param FilterBuilder $filterBuilder
     * @param FilterGroup $filterGroup
     * @param CustomerRepository $customerRepository
     * @param Shoppingminds $shoppingMinds
     */
    public function __construct(
        Context $context,
        searchCriteriaInterface $searchCriteriaInterface,
        FilterBuilder $filterBuilder,
        FilterGroup $filterGroup,
        CustomerRepository $customerRepository,
        Shoppingminds $shoppingMinds
    ) {

        $this->searchCriteriaInterface = $searchCriteriaInterface;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroup = $filterGroup;
        $this->customerRepository = $customerRepository;

        $this->shoppingMinds = $shoppingMinds;

        parent::__construct($context);
    }

    /**
     * Method called via route /{admin}/shoppingminds/customer/export
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute() {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('entity_id')
                ->setConditionType('gt')
                ->setValue(0)
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);
        $customers = $this->customerRepository->getList($this->searchCriteriaInterface);
        $customerItems = $customers->getItems();

        $results = $this->shoppingMinds->scheduleEntitiesForProcessing( $customerItems, Shoppingminds::SHM_CUSTOMER_ENDPOINT );

        if( $results['added'] > 0 ) {
            $this->messageManager->addSuccessMessage( $results['added']." customers are scheduled for export." );
        } else {
            $this->messageManager->addSuccessMessage( "All customers are already scheduled for export." );
        }

        if( $results['rejected'] > 0 ) {
            $this->messageManager->addErrorMessage( $results['rejected']." customers were skipped. With reason: Already in queue.");
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl($this->_redirect->getRefererUrl());
    }
}