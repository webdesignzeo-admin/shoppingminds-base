<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Controller\Adminhtml\General;

use Magento\Sales\Model\OrderRepository;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Catalog\Model\ProductRepository;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

use Shoppingminds\Base\Model\Shoppingminds;

/**
 * Controller to add all ShoppingMinds supported entity ids to the scheduler queue for export.
 * @see execute() method for more information.
 *
 * Class Export
 *
 * @package Shoppingminds\Base\Controller\Adminhtml\General
 */
class Export extends \Magento\Backend\App\Action
{
    protected $_publicActions = ['export'];

    /** @var ProductRepository  */
    protected $productRepository;
    /** @var OrderRepository */
    protected $orderRepository;
    /** @var CustomerRepository */
    protected $customerRepository;

    /** @var SearchCriteriaInterface  */
    protected $searchCriteriaInterface;
    /** @var FilterGroup  */
    protected $filterGroup;
    /** @var FilterBuilder  */
    protected $filterBuilder;

    /** @var Status  */
    protected $productStatus;
    /** @var Visibility */
    protected $productVisibility;

    /** @var Shoppingminds  */
    protected $shoppingMinds;

    /**
     * Export constructor.
     *
     * @param Context $context
     * @param ProductRepository $productRepository
     * @param OrderRepository $orderRepository
     * @param CustomerRepository $customerRepository
     * @param SearchCriteriaInterface $searchCriteriaInterface
     * @param FilterGroup $filterGroup
     * @param FilterBuilder $filterBuilder
     * @param Status $productStatus
     * @param Visibility $productVisibility
     * @param Shoppingminds $shoppingMinds
     */
    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        OrderRepository $orderRepository,
        CustomerRepository $customerRepository,
        SearchCriteriaInterface $searchCriteriaInterface,
        FilterGroup $filterGroup,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisibility,
        Shoppingminds $shoppingMinds
    ) {
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;

        $this->searchCriteriaInterface = $searchCriteriaInterface;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->shoppingMinds = $shoppingMinds;

        parent::__construct($context);
    }

    /**
     * Method called via route /{admin}/shoppingminds/general/export
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $this->insertAllProductsInQueue();
        $this->insertAllOrdersInQueue();
        $this->insertAllCustomerInQueue();

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl($this->_redirect->getRefererUrl());
    }

    /**
     * Adds all active product entity ids to the export queue
     *
     * @see \Shoppingminds\Base\Cron\ExportProductsCommand for export code source
     */
    protected function insertAllProductsInQueue()
    {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('status')
                ->setConditionType('in')
                ->setValue($this->productStatus->getVisibleStatusIds())
                ->create(),
            $this->filterBuilder
                ->setField('visibility')
                ->setConditionType('in')
                ->setValue($this->productVisibility->getVisibleInSiteIds())
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);
        $products = $this->productRepository->getList($this->searchCriteriaInterface);
        $productItems = $products->getItems();
        
        $results = $this->shoppingMinds->scheduleEntitiesForProcessing( $productItems, Shoppingminds::SHM_PRODUCT_ENDPOINT );

        $this->handleMessaging( $results, 'products' );
    }

    /**
     * Adds all active order entity ids to the export queue
     *
     * @see \Shoppingminds\Base\Cron\ExportOrdersCommand for export code source
     */
    public function insertAllOrdersInQueue()
    {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('entity_id')
                ->setConditionType('gt')
                ->setValue(0)
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);
        $orderList = $this->orderRepository->getList($this->searchCriteriaInterface);

        $results = $this->shoppingMinds->scheduleEntitiesForProcessing( $orderList, Shoppingminds::SHM_ORDER_ENDPOINT );

        $this->handleMessaging( $results, 'orders' );
    }

    /**
     * Adds all active customers entity ids to the export queue
     *
     * @see \Shoppingminds\Base\Cron\ExportCustomersCommand for export code source
     */
    protected function insertAllCustomerInQueue()
    {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('entity_id')
                ->setConditionType('gt')
                ->setValue(0)
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);
        $customers = $this->customerRepository->getList($this->searchCriteriaInterface);
        $customerItems = $customers->getItems();
        $results = $this->shoppingMinds->scheduleEntitiesForProcessing( $customerItems, Shoppingminds::SHM_CUSTOMER_ENDPOINT );

        $this->handleMessaging( $results, 'customers' );
    }

    /**
     * Handle results and add output to the message manager for feedback in UI.
     *
     * @param array $results
     * @param $type
     */
    private function handleMessaging( array $results, $type )
    {
        if( $results['added'] > 0 ) {
            $this->messageManager->addSuccessMessage( $results['added']." $type are scheduled for export." );
        } else {
            $this->messageManager->addSuccessMessage( "All $type are already scheduled for export." );
        }

        if( $results['rejected'] > 0 ) {
            $this->messageManager->addErrorMessage( $results['rejected']." $type were skipped. With reason: Already in queue.");
        }
    }
}