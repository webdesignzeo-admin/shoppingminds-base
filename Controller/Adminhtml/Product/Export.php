<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Shopping Minds Nederland B.V. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Shoppingminds\Base\Controller\Adminhtml\Product;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ProductRepository;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

use Shoppingminds\Base\Model\Shoppingminds;

/**
 * Controller to add Product entity ids to the scheduler queue for export.
 * @see \Shoppingminds\Base\Cron\ExportProductsCommand for export code source
 *
 * Class Export
 *
 * @package Shoppingminds\Base\Controller\Adminhtml\Product
 */
class Export extends \Magento\Backend\App\Action
{
    protected $_publicActions = ['export'];

    /** @var ProductRepository  */
    protected $productRepository;

    /** @var SearchCriteriaInterface  */
    protected $searchCriteriaInterface;
    /** @var FilterGroup  */
    protected $filterGroup;
    /** @var FilterBuilder  */
    protected $filterBuilder;

    /** @var Status  */
    protected $productStatus;
    /** @var Visibility */
    protected $productVisibility;

    /** @var Shoppingminds  */
    protected $shoppingMinds;

    /**
     * Export constructor.
     * @param Context $context
     * @param ProductRepository $productRepository
     * @param SearchCriteriaInterface $searchCriteriaInterface
     * @param FilterGroup $filterGroup
     * @param FilterBuilder $filterBuilder
     * @param Status $productStatus
     * @param Visibility $productVisibility
     * @param Shoppingminds $shoppingMinds
     */
    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        SearchCriteriaInterface $searchCriteriaInterface,
        FilterGroup $filterGroup,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisibility,
        Shoppingminds $shoppingMinds
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaInterface = $searchCriteriaInterface;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;

        $this->shoppingMinds = $shoppingMinds;

        parent::__construct($context);
    }

    /**
     * Method called via route /{admin}/shoppingminds/product/export
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('status')
                ->setConditionType('in')
                ->setValue($this->productStatus->getVisibleStatusIds())
                ->create(),
            $this->filterBuilder
                ->setField('visibility')
                ->setConditionType('in')
                ->setValue($this->productVisibility->getVisibleInSiteIds())
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);
        $products = $this->productRepository->getList($this->searchCriteriaInterface);
        $productItems = $products->getItems();

        $results = $this->shoppingMinds->scheduleEntitiesForProcessing( $productItems, Shoppingminds::SHM_PRODUCT_ENDPOINT );

        if( $results['added'] > 0 ) {
            $this->messageManager->addSuccessMessage( $results['added']." products are scheduled for export." );
        } else {
            $this->messageManager->addSuccessMessage( "All products are already scheduled for export." );
        }

        if( $results['rejected'] > 0 ) {
            $this->messageManager->addErrorMessage( $results['rejected']." products were skipped. With reason: Already in queue.");
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl($this->_redirect->getRefererUrl());
    }
}